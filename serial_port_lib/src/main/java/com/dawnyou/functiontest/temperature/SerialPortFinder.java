package com.dawnyou.functiontest.temperature;

import android.util.Log;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.Vector;

public class SerialPortFinder {
    private static final String TAG = "SerialPort";
    private Vector<Driver> mDrivers = null;

    public String[] getAllDevices() {
        Vector localVector = new Vector();
        try {
            Iterator localIterator1 = getDrivers().iterator();
            while (localIterator1.hasNext()) {
                Driver localDriver = (Driver) localIterator1.next();
                Iterator localIterator2 = localDriver.getDevices().iterator();
                while (localIterator2.hasNext()) {
                    localVector.add(String.format("%s (%s)", new Object[]{((File) localIterator2.next()).getName(), localDriver.getName()}));
                }
            }
        } catch (IOException localIOException) {
            localIOException.printStackTrace();
        }
        return (String[]) localVector.toArray(new String[localVector.size()]);
    }

    public String[] getAllDevicesPath() {
        Vector localVector = new Vector();
        try {
            Iterator localIterator1 = getDrivers().iterator();
            while (localIterator1.hasNext()) {
                Iterator localIterator2 = ((Driver) localIterator1.next()).getDevices().iterator();
                while (localIterator2.hasNext()) {
                    localVector.add(((File) localIterator2.next()).getAbsolutePath());
                }
            }
        } catch (IOException localIOException) {
            localIOException.printStackTrace();
        }
        return (String[]) localVector.toArray(new String[localVector.size()]);
    }

    public Vector<Driver> getDrivers()
            throws IOException {
        if (this.mDrivers == null) {
            this.mDrivers = new Vector();
            LineNumberReader localLineNumberReader = new LineNumberReader(new FileReader("/proc/tty/drivers"));
            for (; ; ) {
                String localObject = localLineNumberReader.readLine();
                if (localObject == null) {
                    break;
                }
                String str = localObject.substring(0, 21).trim();
                String[] splitLocalObject = localObject.split(" +");
                if ((splitLocalObject.length >= 5) && (splitLocalObject[(splitLocalObject.length - 1)].equals("serial"))) {
                    StringBuilder localStringBuilder = new StringBuilder();
                    localStringBuilder.append("---串口设备---");
                    localStringBuilder.append(str);
                    localStringBuilder.append(" on ");
                    localStringBuilder.append(splitLocalObject[(splitLocalObject.length - 4)]);
                    Log.d("SerialPort", localStringBuilder.toString());
                    this.mDrivers.add(new Driver(str, splitLocalObject[(splitLocalObject.length - 4)]));
                }
            }
            localLineNumberReader.close();
        }
        return this.mDrivers;
    }

    public class Driver {
        private String mDeviceRoot;
        Vector<File> mDevices = null;
        private String mDriverName;

        public Driver(String paramString1, String paramString2) {
            this.mDriverName = paramString1;
            this.mDeviceRoot = paramString2;
        }

        public Vector<File> getDevices() {
            if (this.mDevices == null) {
                this.mDevices = new Vector();
                File[] arrayOfFile = new File("/dev").listFiles();
                int i = 0;
                if ((arrayOfFile != null) && (arrayOfFile.length != 0)) {
                    i = 0;
                }
                while (i < arrayOfFile.length) {
                    if (arrayOfFile[i].getAbsolutePath().startsWith(this.mDeviceRoot)) {
                        StringBuilder localStringBuilder = new StringBuilder();
                        localStringBuilder.append("查找的设备：");
                        localStringBuilder.append(arrayOfFile[i]);
                        Log.d("SerialPort", localStringBuilder.toString());
                        this.mDevices.add(arrayOfFile[i]);
                    }
                    i += 1;
                    continue;
                }
            }
            return this.mDevices;
        }

        public String getName() {
            return this.mDriverName;
        }
    }
}

