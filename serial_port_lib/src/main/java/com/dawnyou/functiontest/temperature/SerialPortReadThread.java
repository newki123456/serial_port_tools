package com.dawnyou.functiontest.temperature;

import java.io.IOException;
import java.io.InputStream;

public abstract class SerialPortReadThread extends Thread {
    private static final String TAG = SerialPortReadThread.class.getSimpleName();
    private InputStream mInputStream;
    private byte[] mReadBuffer;

    public SerialPortReadThread(InputStream paramInputStream) {
        this.mInputStream = paramInputStream;
        this.mReadBuffer = new byte['?'];
    }

    public abstract void onDataReceived(byte[] paramArrayOfByte);

    public void release() {
        interrupt();
        InputStream localInputStream = this.mInputStream;
        if (localInputStream != null) {
            try {
                localInputStream.close();
                this.mInputStream = null;
                return;
            } catch (IOException localIOException) {
                localIOException.printStackTrace();
            }
        }
    }

    public void run() {
        super.run();
        while (!isInterrupted()) {
            try {
                if (this.mInputStream == null) {
                    return;
                }
                int i = this.mInputStream.read(this.mReadBuffer);
                if (-1 != i) {
                    if (i <= 0) {
                        return;
                    }
                    byte[] arrayOfByte = new byte[i];
                    System.arraycopy(this.mReadBuffer, 0, arrayOfByte, 0, i);
                    onDataReceived(arrayOfByte);
                } else {
                }
            } catch (IOException localIOException) {
                localIOException.printStackTrace();
                return;
            }
        }
    }

    public void start() {
        try {
            super.start();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
