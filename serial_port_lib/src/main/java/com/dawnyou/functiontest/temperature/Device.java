package com.dawnyou.functiontest.temperature;

import java.io.File;
import java.io.Serializable;

public class Device implements Serializable {
    private static final String TAG = Device.class.getSimpleName();
    private File file;
    private String name;
    private String root;

    public Device(String paramString1, String paramString2, File paramFile) {
        this.name = paramString1;
        this.root = paramString2;
        this.file = paramFile;
    }

    public File getFile() {
        return this.file;
    }

    public String getName() {
        return this.name;
    }

    public String getRoot() {
        return this.root;
    }

    public void setFile(File paramFile) {
        this.file = this.file;
    }

    public void setName(String paramString) {
        this.name = paramString;
    }

    public void setRoot(String paramString) {
        this.root = paramString;
    }
}

