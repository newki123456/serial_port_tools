package com.dawnyou.functiontest.temperature;


import java.io.File;

public abstract interface OnOpenSerialPortListener {
    public abstract void onFail(File paramFile, Status paramStatus);

    public abstract void onSuccess(File paramFile);

    public static enum Status {
        NO_READ_WRITE_PERMISSION, OPEN_FAIL;

        private Status() {
        }
    }
}
