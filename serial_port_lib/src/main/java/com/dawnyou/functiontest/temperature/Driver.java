package com.dawnyou.functiontest.temperature;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class Driver {
    private static final String TAG = Driver.class.getSimpleName();
    private String mDeviceRoot;
    private String mDriverName;

    public Driver(String paramString1, String paramString2) {
        this.mDriverName = paramString1;
        this.mDeviceRoot = paramString2;
    }

    public ArrayList<File> getDevices() {
        ArrayList localArrayList = new ArrayList();
        Object localObject = new File("/dev");
        String str;
        StringBuilder localStringBuilder;
        if (!((File) localObject).exists()) {
            str = TAG;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("getDevices: ");
            localStringBuilder.append(((File) localObject).getAbsolutePath());
            localStringBuilder.append("不存在");
            Log.i(str, localStringBuilder.toString());
            return localArrayList;
        }
        if (!((File) localObject).canRead()) {
            str = TAG;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("getDevices: ");
            localStringBuilder.append(((File) localObject).getAbsolutePath());
            localStringBuilder.append("没有读取权限");
            Log.i(str, localStringBuilder.toString());
            return localArrayList;
        }
        File[] files = ((File) localObject).listFiles();
        int i = 0;
        while (i < files.length) {
            if (files[i].getAbsolutePath().startsWith(this.mDeviceRoot)) {
                str = TAG;
                localStringBuilder = new StringBuilder();
                localStringBuilder.append("Found new device: ");
                localStringBuilder.append(files[i]);
                Log.d(str, localStringBuilder.toString());
                localArrayList.add(files[i]);
            }
            i += 1;
        }
        return localArrayList;
    }

    public String getName() {
        return this.mDriverName;
    }
}
