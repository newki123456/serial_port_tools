package com.dawnyou.functiontest.temperature;

public abstract interface OnSerialPortDataListener {
    public abstract void onDataReceived(byte[] paramArrayOfByte);

    public abstract void onDataSent(byte[] paramArrayOfByte);
}
