package com.dawnyou.functiontest.temperature;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class SerialPortManager extends SerialPort {
    private static final String TAG = SerialPortManager.class.getSimpleName();
    private FileDescriptor mFd;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;
    private OnOpenSerialPortListener mOnOpenSerialPortListener;
    private OnSerialPortDataListener mOnSerialPortDataListener;
    private Handler mSendingHandler;
    private HandlerThread mSendingHandlerThread;
    private SerialPortReadThread mSerialPortReadThread;
    private Timer timer;

    private void startReadThread() {
        this.mSerialPortReadThread = new SerialPortReadThread(this.mFileInputStream) {
            public void onDataReceived(byte[] paramAnonymousArrayOfByte) {
                if (SerialPortManager.this.mOnSerialPortDataListener != null) {
                    SerialPortManager.this.mOnSerialPortDataListener.onDataReceived(paramAnonymousArrayOfByte);
                }
            }
        };
        this.mSerialPortReadThread.start();
    }

    private void startSendThread() {
        this.mSendingHandlerThread = new HandlerThread("mSendingHandlerThread");
        this.mSendingHandlerThread.start();
        this.mSendingHandler = new Handler(this.mSendingHandlerThread.getLooper()) {
            public void handleMessage(Message paramAnonymousMessage) {
                byte[] byteMessages = (byte[]) paramAnonymousMessage.obj;
                if ((SerialPortManager.this.mFileOutputStream != null) && (byteMessages != null) &&
                        (byteMessages.length > 0)) {
                    try {
                        SerialPortManager.this.mFileOutputStream.write(byteMessages);
                        if (SerialPortManager.this.mOnSerialPortDataListener != null) {
                            SerialPortManager.this.mOnSerialPortDataListener.onDataSent(byteMessages);
                        }
                        return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private void stopReadThread() {
        SerialPortReadThread localSerialPortReadThread = this.mSerialPortReadThread;
        if (localSerialPortReadThread != null) {
            localSerialPortReadThread.release();
        }
    }

    private void stopSendThread() {
        this.mSendingHandler = null;
        HandlerThread localHandlerThread = this.mSendingHandlerThread;
        if (localHandlerThread != null) {
            localHandlerThread.interrupt();
            this.mSendingHandlerThread.quit();
            this.mSendingHandlerThread = null;
        }
    }

    public void beginBytes(final byte[] paramArrayOfByte) {
        this.timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                SerialPortManager.this.sendBytes(paramArrayOfByte);
            }
        };
        this.timer.schedule(timerTask, 0L, 1000L);
    }

    public void closeSerialPort() {
        if (this.mFd != null) {
            close();
            this.mFd = null;
        }
        stopSendThread();
        stopReadThread();
        FileInputStream localFileInputStream = this.mFileInputStream;
        if (localFileInputStream != null) {
            try {
                localFileInputStream.close();
            } catch (IOException localIOException1) {
                localIOException1.printStackTrace();
            }
            this.mFileInputStream = null;
        }
        FileOutputStream localFileOutputStream = this.mFileOutputStream;
        if (localFileOutputStream != null) {
            try {
                localFileOutputStream.close();
            } catch (IOException localIOException2) {
                localIOException2.printStackTrace();
            }
            this.mFileOutputStream = null;
        }
        this.mOnOpenSerialPortListener = null;
        this.mOnSerialPortDataListener = null;
    }

    public boolean openSerialPort(File paramFile, int paramInt) {
        Object localObject = TAG;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("openSerialPort: ");
        localStringBuilder.append(String.format("打开串口 %s - 波特率 %s", new Object[]{paramFile.getPath(), Integer.valueOf(paramInt)}));
        Log.i((String) localObject, localStringBuilder.toString());
        if (((!paramFile.canRead()) || (!paramFile.canWrite())) && (!chmod666(paramFile))) {
            Log.i(TAG, "没有读写权限");
            localObject = this.mOnOpenSerialPortListener;
            if (localObject != null) {
                ((OnOpenSerialPortListener) localObject).onFail(paramFile, OnOpenSerialPortListener.Status.NO_READ_WRITE_PERMISSION);
            }
            return false;
        }
        try {
            this.mFd = open(paramFile.getAbsolutePath(), paramInt, 0);
            this.mFileInputStream = new FileInputStream(this.mFd);
            this.mFileOutputStream = new FileOutputStream(this.mFd);
            localObject = TAG;
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("openSerialPort: 串口已经打开");
            localStringBuilder.append(this.mFd);
            Log.i((String) localObject, localStringBuilder.toString());
            if (this.mOnOpenSerialPortListener != null) {
                this.mOnOpenSerialPortListener.onSuccess(paramFile);
            }
            startSendThread();
            startReadThread();
            return true;
        } catch (Exception localException) {
            localException.printStackTrace();
            OnOpenSerialPortListener localOnOpenSerialPortListener = this.mOnOpenSerialPortListener;
            if (localOnOpenSerialPortListener != null) {
                localOnOpenSerialPortListener.onFail(paramFile, OnOpenSerialPortListener.Status.OPEN_FAIL);
            }
        }
        return false;
    }

    public boolean sendBytes(byte[] paramArrayOfByte) {
        if ((this.mFd != null) && (this.mFileInputStream != null) && (this.mFileOutputStream != null) && (this.mSendingHandler != null)) {
            Message localMessage = Message.obtain();
            localMessage.obj = paramArrayOfByte;
            return this.mSendingHandler.sendMessage(localMessage);
        }
        return false;
    }

    public SerialPortManager setOnOpenSerialPortListener(OnOpenSerialPortListener paramOnOpenSerialPortListener) {
        this.mOnOpenSerialPortListener = paramOnOpenSerialPortListener;
        return this;
    }

    public SerialPortManager setOnSerialPortDataListener(OnSerialPortDataListener paramOnSerialPortDataListener) {
        this.mOnSerialPortDataListener = paramOnSerialPortDataListener;
        return this;
    }

    public void stopBytes() {
        Timer localTimer = this.timer;
        if (localTimer != null) {
            localTimer.cancel();
            this.timer = null;
        }
    }
}
