package com.dawnyou.functiontest.temperature;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;

public class SerialPort {
    private static final String TAG = "lock_serial_port";

    static {
        System.loadLibrary("serial_port");
    }

    public static native FileDescriptor open(String paramString, int paramInt1, int paramInt2);

    boolean chmod666(File paramFile) {
        if (paramFile != null) {
            if (!paramFile.exists()) {
                return false;
            }
            try {
                Process localProcess = Runtime.getRuntime().exec("/system/bin/su");
                Object localObject = new StringBuilder();
                ((StringBuilder) localObject).append("chmod 666 ");
                ((StringBuilder) localObject).append(paramFile.getAbsolutePath());
                ((StringBuilder) localObject).append("\nexit\n");
                localObject = ((StringBuilder) localObject).toString();
                localProcess.getOutputStream().write(((String) localObject).getBytes());
                if ((localProcess.waitFor() == 0) && (paramFile.canRead()) && (paramFile.canWrite())) {
                    boolean bool = paramFile.canExecute();
                    if (bool) {
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        }
        return false;
    }

    public native void close();
}
