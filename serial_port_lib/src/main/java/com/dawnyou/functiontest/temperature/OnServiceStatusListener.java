package com.dawnyou.functiontest.temperature;


public abstract interface OnServiceStatusListener {
    public abstract void OnHeartStatus(boolean paramBoolean);

    public abstract void OnOpenDoor(byte[] paramArrayOfByte);

    public abstract void OnSerialPort(boolean paramBoolean);
}
