**远程依赖**
```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}

implementation 'com.gitee.newki123456:serial_port_tools:1.0.1'
```
**使用：**
```kotlin
    private lateinit var mSerialPortManager: SerialPortManager
    var cmd = byteArrayOf(-91, 85, 1, -5)  //开启通信的指令


     //具体调用就是2个回调，设置串口，开启指令
     mSerialPortManager = SerialPortManager()
        mSerialPortManager
            .setOnOpenSerialPortListener(object : OnOpenSerialPortListener {
                override fun onFail(paramFile: File?, paramStatus: OnOpenSerialPortListener.Status) {
                    Toast.makeText(mContext,paramStatus.toString(),Toast.LENGTH_SHORT).show()
                }

                override fun onSuccess(paramFile: File) {

                }
            })
                //设置串口的数据通信回调
            .setOnSerialPortDataListener(object : OnSerialPortDataListener {
                override fun onDataReceived(paramAnonymousArrayOfByte: ByteArray) {
                    //解析返回的数据转换为摄氏度
                    val i = paramAnonymousArrayOfByte[3]
                    val f = (paramAnonymousArrayOfByte[2] + i * 255 + 20) / 100.0f
                    val message = Message.obtain()
                    message.obj = java.lang.Float.valueOf(f)
                    message.what = 1
                    mHandler.sendMessage(message)
                }

                override fun onDataSent(paramArrayOfByte: ByteArray?) {

                }
            })
            .openSerialPort(File("dev/ttyS3"), 115000)  //打开指定串口,输入端口号和比特率

        mSerialPortManager.beginBytes(cmd)  //开启读取
```
注意Destory释放资源：
```
 override fun onDestroy() {
        super.onDestroy()
        //关闭串口释放资源
        mSerialPortManager.stopBytes()
        mSerialPortManager.closeSerialPort()
    }

```